// Mathematical Operations (-, *, /, %)
// Subtraction
let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3);//-0.5 results in proper mathematical operation
console.log(num3-num4);//5 results in proper mathematical operation
console.log(numString1-num2);//-1 results in proper mathematical operation because the string was forced to become a number
console.log(numString2-num2);//0 
console.log(numString1-numString2);//-1 In subtraction, numeric strings will not concatenate and instead will be forcibly changed its type and subtract properly

let sample2 = "Juan Dela Cruz";

console.log(sample2-numString1);//NaN - results in not a number. When trying to perform subtraction between alphanumeric string and numeric string, the result is NaN.

// Multiplication
console.log(num1*num2);//30
console.log(numString1*num1);//25
console.log(numString1*numString2);//30
console.log(sample2 * 6);

let product1 = num1 * num2;
let product2 = numString1 * num1;
let product3 = numString1 * numString2;
console.log(product1);

// Division
console.log(product1/num2);//5
console.log(product2/5);//5
console.log(numString2 / numString1);//1.2
console.log(numString2 % numString1);//1

// division/multiplication by 0
console.log(product2 * 0)//0
console.log(product3 * 0)//0
// Division by 0 is not accurately and should not be done, it results to infinity

// % Modulo - remainder of division operation
console.log(product2 % num2);//product2/num2 (25/6) = remainder - 1
console.log(product3 % product2);//30/25 = remainder - 5
console.log(num1 % num2);//5
console.log(num1 % num1);//0

// Boolean (true or false)
/*
	Boolean is usually used for logic operations or if-else condition
		-boolean values are normally used to store values relating to the state of certain things.
	When creating a variable which will contain a boolean, the variable name is usually a yes or no question
*/	

	let isAdmin = true;
	let isMarried = false;
	let isMVP = true;

	// You can also concatenate strings + boolean\
	console.log("Is she married? " + isMarried);
	console.log("Is he the MVP? " + isMVP);
	console.log(`Is he the current Admin?` + isAdmin);

// Arrays
/*
	Arrays
		- arrays are a special kind of data type to store multiple values
		- can actually store data with diffrent types BUT as the best practice, arrays are used to contain multiple values of the SAME data type
		- values in an arrays are separated by commas 
		An Array is created with an array literal = []
*/

//Syntax:
	// let/const arrayName = [elementA, elementB, elementC, ...];
let array1 = ["Goku", "Piccolo", "Gohan", "Vegeta"];
console.log(array1);
let array2 = ["One Punch Man", true, 500, "Saitama"];
console.log(array2);

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Array are better thought of as a group data

// Objects
/*
	Objects
		- are another special kind of data type used to mimic the real world
		- used to create complex data that contain pieces of information that are relevant to each other
		- Objects are create with object literals = {}
			- each data/value are paired with a key
			- each field is called a property
			- each field is separated comma

		syntax:
			let/const objectName = {
				propertyA: value,
				propertyB: value
			}
*/
let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+639154847451", "8123 4567"],
	address: {
		houseNumber: "345",
		street: "Diamond",
		city: "Manila"
	}
};

console.log(person);
/*Mini-Activity
	Create a variable with a group of data
		- The group of data should contain names from your favorite band/idol

	Create a variable which contain multiple values of differing types and describe a single person
		- This data type should be able to contain multiple key value pairs:
			firstName: <value>
			lastName: <value>
			isDeveloper: <value>
			hasPortfolio: <value>
			age: <value>
			contact: <value>, <value>
			address:
				houseNumber: <value>
				street: <value>
				city: <value>
*/

let TheGentlemen = {
	firstMember: "Taz Bentley",
	secondMember: "Dave Dederer",
	thirdMember: "Jason Finn",
	fourthMember: "Dez Cadena",
	fifthMember: "Duff McKagan",
	favoriteMember: "Taz Bentley",
}
console.log(TheGentlemen);

let me = {
	firstName: "Gabriel",
	lastName: "Naty",
	isDeveloper: false,
	hasPortfolio: false,
	age: 20,
	contact: ["+639123456879", "9823 1237"],
	address: {
		houseNumber: "4293",
		street: "Sunflower",
		city: "Imus"
	}
};
console.log(me);

// Undefine vs Null
	// Null - is explicit absence of data/value. This is done to protect that a variable contains nothing over undefined as undefined merely means there is no data in the variable BECAUSE the variable has not been assigned an initial value.

	let sampleNull = null;

	// Undefined - is a representation that a variable has been declared but it was not assigned an initial values

	let sampleUndefined;

	console.log(sampleNull);
	console.log(sampleUndefined);

	// certain processes in programmin explicitly return null to indicate that the task resulted to nothing

	let foundResult = null;

	// Example:
	let myNumber = 0;
	let myString = " ";
	// using null to compare to a 0 value and an empty string is much better for readability

	// for undefined, this is normally caused by developers creating variables that have no value or data/associated with them.
	// This is when a variable does exist but its value is still unknown
	let person2 = {
		name: "Peter",
		age: 35
	}

	// because person2 does exist but the property isAdmin does not.
	console.log(person2.isAdmin);





	/* [SECTION] Functions
		Functions
			- in JavaScript are line/block of codes that tell our device/application to perform a certain task when called/invoked
			- are reusable pieces of code with instructions which used over and over again just as long as we can call/invoke them

			Syntax:
				function functionName(){
					code block
						-the block of code that will be executed once the function has been run/called/invoked
				}

	*/

function printName1(){
	console.log("My name is Jin");
}

// invoking/calling of function - functionName()
printName1();
printName1();
printName1();


function showSum(){
	console.log(25+6);
}

showSum();
// Note: do not create functions with the same name
// invoking functions with the same name outputs the latest function

/* Parameters and Arguments
	"name" is called a parameter
		a parameter acts a named variable/container that exists ONLY inside of the function. This is used as to store information/ to act as a stand-in or the container the value passed into the function as an argument.
*/

function printName(name){
	console.log(`My name is ${name}`);
}

// When a fucntion is invoked and data is passed, we call the data as argument
// In this invocation, "V" is an argument passed inot our printName function and is represented by the "name" parameter within our function.
// data passed into the function: argument
// representation of the argument within the function: parameter
printName("V");

function displayNum(number){
	alert(number)
};

displayNum(1000);

/* Mini-Activity
	Create a function which will be able to show message in the console.
		The message should be able to be passed into function via an argument

			- Sample Message:
				"JavaScript is Fun"
				"C++ is Fun"

				argument: programming language
*/

function showMessage(x){
	console.log(`${x} is Fun`);
}

programminglanguage = "JavaScript";
showMessage(programminglanguage);
programminglanguage = "C++";
showMessage(programminglanguage);

// Multiple parameters and arguments
function displayFullName(firstName, mi, lastName, age){
	console.log(`${firstName} ${mi} ${lastName} ${age}`);
};

displayFullName("Seok-jin", "P", "Kim", 29);

// return keyword
	// The "return" statement allows the output of a function to be passed to the line/block of code that invoked/called the function
	// any line/block of code that comes after the return statement is ignored because it ends the function execution

function createFullName(firstName, middleName, lastName){
	// return keyword is used so that a function may return a value
	// it also stops the process of the function any other instruction after the keyword will not be processed
	return `${firstName} ${middleName} ${lastName}`
	console.log("I will no longer run because the funcion's value/result has been returned.");
};

let fullName1 = createFullName("Jungkook", "BTS", "Jeon")

console.log(fullName1);